import sys, time, os, subprocess, pathlib
from os.path import isfile, join

# ------------ Start of parameters, you can adjust these without worrying too much.
# Target
pcapFile = sys.argv[1]

# Tempfiles, make sure they do not already exist or you will lose data!
tmpPcapFile    = "tmpcapfile_1628175615.cap"
altTmpPcapFile = "tmpcapfile_9846536213.cap"

# Needs a '/' at the end!
resultsDirectory = "results/"
# ------------ End Parameters 



# The resultsDirectory needs to exist
pathlib.Path(resultsDirectory).mkdir(parents=True, exist_ok=True)


# Makes sure weird AP names don't give us trouble
def decodeIgnoringErrors(shellOutput):
    msg_content = ''
    for line in shellOutput.stdout:
        l = line.decode(encoding="utf-8", errors="ignore")
        msg_content += l
    shellOutput.wait()
    return msg_content 

print("#-------- PCAP Wi-Fi Valuables Extractor --------#")  
print("Starting extraction on " + pcapFile +"!")
print("#----------- Reading Previous Results -----------#")  
readingPreviousStart = time.time()

# Get a list of all the existing crackable APs in the crackable folder
knownCrackableFiles = [f for f in os.listdir(resultsDirectory) if isfile(join(resultsDirectory, f))]
if knownCrackableFiles != None:
  print("Found previous files: " + str(knownCrackableFiles))

# Create a dictionary of the previously known crackables:
knownCrackableAPDict = {}
for fileName in knownCrackableFiles:
  # First 17 chars are BSSID
  bssid = fileName[:17]
  # Chars between BSSID & .cap extension are SSID
  ssid = fileName[18:-4]
  knownCrackableAPDict[bssid]=ssid

readingPreviousTime = round((time.time() - readingPreviousStart),2)  
print("Completed in: " + str(readingPreviousTime) + " seconds.")


print("#---------- Looking For New Crackables ----------#")
aircrackStart = time.time()

analysisCmd = "echo 'nope' | aircrack-ng '" + pcapFile + "'"
anylysisOutput = decodeIgnoringErrors(subprocess.Popen(analysisCmd,shell=True,stdout=subprocess.PIPE))

aircrackTime = round((time.time() - aircrackStart),2) 
print("Completed in: " + str(aircrackTime) + " seconds.")


print("#-------- Processing Aircrack-ng Results --------#")
aircrackProcessStart = time.time()

lineIndex = 0
# Set of tuples (BSSID,SSID)
APsWithCrackables = set()
for line in anylysisOutput.split('\n'):
  lineIndex += 1
  # Ignore first 6 lines
  if lineIndex <= 6:
    continue
  # We're done when we encounter an empty line:
  if line == "":
    break
  
  # lstrip for removing extra leading spaces at the start
  splitLine = line.lstrip().split("  ")
  bssid = splitLine[1]
  ssid = splitLine[2].replace(" ","_")
  elements = [i.strip() for i in splitLine if i != ""]
  #print(elements)
  
  # Use last element of parsed list to determine if we can extract anything of value:
  switchVar = elements[-1].split()
    
  # Type of encryption is unknown AND we didn't catch anything of value:
  if switchVar[-1] == "Unknown" or switchVar[-1] == "EAPOL+Unknown":
    continue
    
  # WPA(2) encryption was identified 
  elif switchVar[-1] == "handshake)":
    #print("Identified WPA(2)")
    numberOfCrackables = int(switchVar[-2][1:])
    if numberOfCrackables != 0:
      APsWithCrackables.add((bssid,ssid))
      print(ssid + "[" + bssid + "]" + ": " + str(numberOfCrackables) + " handshake(s)!")
      
  # WPA(2) encryption was identified with at least a PMKID    
  elif switchVar[-1] == "PMKID)":
    numberOfCrackables = int(switchVar[-4][1:])
    APsWithCrackables.add((bssid,ssid))
    print(ssid + "[" + bssid + "]" + ": " + str(numberOfCrackables) + " handshake(s) and PMKID!")
    
  # WEP encryption was identified
  elif switchVar[-1] == "IVs)":
    numberOfCrackables = int(switchVar[-2][1:])
    if numberOfCrackables != 0:
      APsWithCrackables.add((bssid,ssid))
      print(ssid + "[" + bssid + "]" + ": " + str(numberOfCrackables) + " IV(s)!")
  
  else: 
    print ("#### FAILED TO PARSE: " + str(elements) + "---------------------------------------------------------")

aircrackProcessTime = round((time.time() - aircrackProcessStart),2) 
print("Completed in: " + str(aircrackProcessTime) + " seconds.")


print("#---------- Starting Packet Extraction ----------#")
packetExtractionStart = time.time()

print("All APs with a crackables: " + str(APsWithCrackables))
crackablesForKnownTargets = 0
crackablesForNewTargets = 0
for APT in APsWithCrackables:
  bssid = APT[0] 
  ssid = APT[1]
  print("Extracting " + ssid + "[" + bssid + "]")
  # Extract all relevant traffic from original pcap file to temporary pcap file
  extractCmd = "tshark -r '" + pcapFile + "' -R '(wlan.fc.type_subtype == 0x08 || wlan.fc.type_subtype == 0x05 || eapol) && wlan.addr == " + bssid + "' -2 -w '" +  tmpPcapFile + "' -F pcap"
  extractionProcess = subprocess.Popen(extractCmd, shell=True, stdout=subprocess.PIPE)
  extractionProcess.wait()
  
  # TODO: fix WEP valuables extraction,
  #   -> Two-step: check if file was empty, if so run tshark again, only filtering out traffic with its wlan.addr?

  
  # Check if we already collected data on the BSSID:
  if bssid in knownCrackableAPDict:
    crackablesForKnownTargets += 1
    # Recreate filename
    fileName = bssid + "-" + knownCrackableAPDict[bssid] + ".cap"
    # Check if we collected a new/additional APs:
    if not ssid in knownCrackableAPDict[bssid]:
      # If this AP didn't have a known SSID, add it:
      if knownCrackableAPDict[bssid] == '': 
        knownCrackableAPDict[bssid] = ssid
        #print("Setting new SSID to know BSSID: " + ssid)
      else:
        # If the AP already carried one SSID, append it: 
        knownCrackableAPDict[bssid] =  knownCrackableAPDict[bssid] + "|" + ssid
      # Apply new filename with new SSID:
      newFileName = bssid + "-" + knownCrackableAPDict[bssid] + ".cap"
      #print("Renaming file: " + fileName + " -> " + newFileName)
      os.rename(resultsDirectory + fileName, resultsDirectory + newFileName)
      fileName = newFileName
  
    # Concatenate packets to existing file:
    concatCmd = "mergecap -F pcap -a " + tmpPcapFile + " " + resultsDirectory + fileName + " -w " + altTmpPcapFile
    concatProcess = subprocess.Popen(concatCmd, shell=True, stdout=subprocess.PIPE)
    concatProcess.wait()
    # Overwrite old file with the result of concatenation
    os.rename(altTmpPcapFile, resultsDirectory + fileName)
    
    # Clean up merged tmpPcapFile
    os.remove(tmpPcapFile)
  else:
    # If no file exists, move extracted packets (in tempfile) into place:
    crackablesForNewTargets += 1
    # Create filename
    fileName = bssid + "-" + ssid + ".cap"
    os.rename(tmpPcapFile, resultsDirectory + fileName)
    print("Written new file: " + fileName)

packetExtractionTime = round((time.time() - packetExtractionStart),2) 
print("Completed in: " + str(packetExtractionTime) + " seconds.")


print("#-------------------- Result --------------------#")
print("Extracted crackables for " + str(crackablesForNewTargets) + " new targets.")
print("Extracted crackables for " + str(crackablesForKnownTargets) + " known targets.")
print("Full extraction time: " + str(readingPreviousTime + aircrackTime + aircrackProcessTime + packetExtractionTime) + " seconds.")
print("#------------------------------------------------#")
