# PCAP Wi-Fi Crackables Extractor

A python script that extracts WPA(2) handshakes and WEP IVs from (large) pcap files by combining aicrack-ng and tshark.

It is suitable for long-term, low-profile operations as it will merge handshakes, IVs and SSIDs it finds across multiple runs.



`THIS TOOL IS FOR LEGAL PURPOSES ONLY!`
